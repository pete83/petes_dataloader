package com.pete.playground.beanconf.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pete on 2015-01-05.
 */
@Configuration
public class PetesApplicationConfiguration {

    @Bean
    public Person person() {
        System.out.println("Returning new person: ");
        return new Person("John", "Doe");
    }
}
