package com.pete.playground.beanconf;

import com.pete.playground.beanconf.beans.Person;
import com.pete.playground.beanconf.beans.PetesApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pete on 2015-01-05.
 */
public class MainBeanPlayground {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(PetesApplicationConfiguration.class);
        Person personBean = ctx.getBean(Person.class);
        System.out.println("personBean: "+personBean);
        System.out.println("personBean: "+ctx.getBean(Person.class));
    }
}
