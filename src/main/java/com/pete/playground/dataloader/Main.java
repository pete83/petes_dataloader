package com.pete.playground.dataloader;

import com.pete.playground.dataloader.beans.AirlineScopeExecutor;
import com.pete.playground.dataloader.beans.DataLoaderConfiguration;
import com.pete.playground.dataloader.business.DataLoaderServices;
import org.apache.commons.lang3.time.StopWatch;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
@EnableConfigurationProperties
@ComponentScan
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOGGER.info("Starting Service Engine Data Loader...");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ConfigurableApplicationContext appContext = SpringApplication.run(Main.class, args);
        DataLoaderServices dataLoaderServices = appContext.getBean("dataLoaderServices", DataLoaderServices.class);
        DataLoaderConfiguration dataLoaderConfiguration = appContext.getBean("dataLoaderConfiguration", DataLoaderConfiguration.class);
        LOGGER.info("Airlines enabled for data loading: {}", dataLoaderConfiguration.getEnabledAirlines());
        for (String enabledAirlineCode : dataLoaderConfiguration.getEnabledAirlines()) {
            AirlineScopeExecutor.doAsAirline(enabledAirlineCode, () -> {
                try {
                    dataLoaderServices.loadEntireDatabase();
                } catch (Exception e) {
                    LOGGER.info("Unable to load airline configuration for " + enabledAirlineCode + " due to " + e.getMessage());
                }
                return null;
            });
        }
        stopWatch.stop();
        LOGGER.info("Service Engine Data Loader has finished its execution. It took {} ms", stopWatch.getTime());
    }

}
