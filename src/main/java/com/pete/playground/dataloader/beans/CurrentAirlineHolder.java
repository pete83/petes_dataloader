package com.pete.playground.dataloader.beans;


import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Created by pete on 2015-01-06.
 */
public class CurrentAirlineHolder {
    private static final ThreadLocal<String> CURRENT_AIRLINE_CODE = new ThreadLocal<>();

    public static void setCurrentAirlineCode(String currentAirlineCode) {
        CURRENT_AIRLINE_CODE.set(currentAirlineCode.toUpperCase());
    }

    public static void resetCurrentAirlineCode() {
        CURRENT_AIRLINE_CODE.set(null);
    }

    public static boolean isAirlineSet() {
        return !isBlank(CURRENT_AIRLINE_CODE.get());
    }

    public static String getCurrentAirlineCode() {
        if (isBlank(CURRENT_AIRLINE_CODE.get())) {
            throw new RuntimeException("Airline code has not been initialized");
        }
        return CURRENT_AIRLINE_CODE.get();
    }
}
