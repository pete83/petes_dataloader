package com.pete.playground.dataloader.beans;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class DataSource {
    private String airline;
    private String url;
    private String username;
    private String password;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline.toUpperCase();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("airline", airline)
                .append("url", url)
                .append("username", username)
                .append("password", password)
                .toString();
    }
}