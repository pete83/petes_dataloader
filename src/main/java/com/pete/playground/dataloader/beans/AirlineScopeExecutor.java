package com.pete.playground.dataloader.beans;

import java.util.concurrent.Callable;

/**
 * Created by pete on 2015-01-08.
 */
public class AirlineScopeExecutor {
    public static Object doAsAirline(String airlineCode, Callable action) {
        try {
            CurrentAirlineHolder.setCurrentAirlineCode(airlineCode);
            return action.call();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            CurrentAirlineHolder.resetCurrentAirlineCode();
        }
    }
}
