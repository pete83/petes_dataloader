package com.pete.playground.dataloader.beans;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pete on 2015-01-06.
 */
@Component
@ConfigurationProperties
public class DataLoaderConfiguration {
    private List<String> enabledAirlines = new ArrayList<>();
    private List<DataSource> dataSources;

    public List<String> getEnabledAirlines() {
        return enabledAirlines;
    }

    public void setEnabledAirlines(List<String> enabledAirlines) {
        for (String enabledAirline : enabledAirlines) {
            this.enabledAirlines.add(enabledAirline.toUpperCase());
        }
    }

    public List<DataSource> getDataSource() {
        return dataSources;
    }

    public List<DataSource> getEnabledDataSources() {
        List<DataSource> enabledDataSources = new ArrayList<>();
        for (DataSource dataSource : dataSources) {
            if (enabledAirlines.contains(dataSource.getAirline())) {
                enabledDataSources.add(dataSource);
            }
        }
        return enabledDataSources;
    }

    public void setDataSource(List<DataSource> dataSources) {
        this.dataSources = dataSources;
    }


}
