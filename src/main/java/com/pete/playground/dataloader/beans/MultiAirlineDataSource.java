package com.pete.playground.dataloader.beans;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.Map;

/**
 * Created by pete on 2015-01-06.
 */
public class MultiAirlineDataSource extends AbstractRoutingDataSource {

    @SuppressWarnings("unchecked")
    public MultiAirlineDataSource(Map<String, DataSource> airlineDataSources) {
        this.setTargetDataSources((Map) airlineDataSources);
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return CurrentAirlineHolder.getCurrentAirlineCode();
    }
}
