package com.pete.playground.dataloader.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Pete.Sterna@sabre.com
 *         Created on 2015-01-08.
 */
public class MultiAirlineEntityManagerFactoryCreator {

    private static final Logger LOGGER = LoggerFactory.getLogger(MultiAirlineEntityManagerFactoryCreator.class);

    private Map<String, LocalContainerEntityManagerFactoryBean> airlineEntityManagerFactoryBeans = new HashMap<>();

    public MultiAirlineEntityManagerFactoryCreator(javax.sql.DataSource multiAirlineDataSource, List<DataSource> airlineDataSources) {
        for (DataSource airlineDataSource : airlineDataSources) {
            LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
            HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
            vendorAdapter.setGenerateDdl(true);
            vendorAdapter.setDatabasePlatform("org.hibernate.dialect.Oracle10gDialect");
            localContainerEntityManagerFactoryBean.setPersistenceUnitName(airlineDataSource.getAirline() + "_PERSISTENCE_UNIT");
            localContainerEntityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
            localContainerEntityManagerFactoryBean.setPackagesToScan("com.pete.playground.dataloader.entities");
            localContainerEntityManagerFactoryBean.setDataSource(multiAirlineDataSource);
            CurrentAirlineHolder.setCurrentAirlineCode(airlineDataSource.getAirline());
            localContainerEntityManagerFactoryBean.afterPropertiesSet();
            airlineEntityManagerFactoryBeans.put(airlineDataSource.getAirline(), localContainerEntityManagerFactoryBean);
        }
        CurrentAirlineHolder.resetCurrentAirlineCode();
    }

    public EntityManagerFactory buildMultiAirlineEntityManagerFactory() throws Exception {
        EntityManagerFactory entityManagerFactory = (EntityManagerFactory) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{EntityManagerFactory.class},
                (proxy, method, args) -> {
                    if (!CurrentAirlineHolder.isAirlineSet() && method.getName().equals("hashCode")) {
                        LOGGER.debug("Method {} invoked on EntityManagerFactory proxy without an airline context {} ", method.getName());
                        //This is ugly, but I wanted to suppress the annoying message at Spring context shutdown.
                        //It's possible to resolve this by somehow overriding the PersistenceAnnotationBeanPostProcessor,
                        //but it proved to be difficult.
                        return 123123123;
                    }
                    LOGGER.debug("Method {} invoked on EntityManagerFactory proxy for airline {} ", method.getName(), CurrentAirlineHolder.getCurrentAirlineCode());
                    LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = airlineEntityManagerFactoryBeans.get(CurrentAirlineHolder.getCurrentAirlineCode());
                    EntityManagerFactory emf = localContainerEntityManagerFactoryBean.getObject();

                    if (method.getName().equals("toString")) {
                        // Deliver toString without touching a target EntityManager.
                        return "MultiAirline EntityManagerFactory proxy for target factory [" + emf + "]";
                    }
                    if (method.getName().equals("createEntityManager")) {
                        return emf.createEntityManager();
                    }
                    if (method.getName().equals("hashCode")) {
                        return emf.hashCode();
                    }
                    if (method.getName().equals("equals")) {
                        // Only consider equal when proxies are identical.
                        return (proxy == args[0]);
                    }
                    LOGGER.warn("Method {} probably not handled properly", method.getName());
                    return emf;
                });
        LOGGER.debug("Created EntityManagerFactory proxy {}", entityManagerFactory.getClass());
        return entityManagerFactory;
    }
}
