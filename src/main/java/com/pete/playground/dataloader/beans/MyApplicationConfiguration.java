package com.pete.playground.dataloader.beans;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * This gets discovered automatically thanks to the @ComponentScan in the Main class
 */
@Configuration
public class MyApplicationConfiguration {

    @Autowired
    private DataLoaderConfiguration dataLoaderConfiguration;

    private Map<String, DataSource> initializeAirlineDataSources() {
        Map<String, DataSource> dataSourceMap = new HashMap<>();
        for (com.pete.playground.dataloader.beans.DataSource dataSourceConfig : dataLoaderConfiguration.getEnabledDataSources()) {
            BasicDataSource dataSource = new BasicDataSource();
            dataSource.setUrl(dataSourceConfig.getUrl());
            dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
            dataSource.setUsername(dataSourceConfig.getUsername());
            dataSource.setPassword(dataSourceConfig.getPassword());
            dataSourceMap.put(dataSourceConfig.getAirline(), dataSource);
        }
        System.out.println("DS registered");
        return dataSourceMap;
    }

    public DataSource dataSource() {
        AbstractRoutingDataSource dataSource = new MultiAirlineDataSource(initializeAirlineDataSources());
        dataSource.afterPropertiesSet();
        return dataSource;
    }

    @Bean
    public EntityManagerFactory localContainerEntityManagerFactoryBean() throws Exception {
        return new MultiAirlineEntityManagerFactoryCreator(dataSource(),
                dataLoaderConfiguration.getEnabledDataSources()).buildMultiAirlineEntityManagerFactory();

      /*  LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        vendorAdapter.setDatabasePlatform("org.hibernate.dialect.Oracle10gDialect");
        localContainerEntityManagerFactoryBean.setPersistenceUnitName("EY" + "_PERSISTENCE_UNIT");
        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
        localContainerEntityManagerFactoryBean.setPackagesToScan("com.pete.playground.dataloader.entities");
        localContainerEntityManagerFactoryBean.setDataSource(dataSource());
        CurrentAirlineHolder.setCurrentAirlineCode("EY");
        localContainerEntityManagerFactoryBean.afterPropertiesSet();
        CurrentAirlineHolder.setCurrentAirlineCode(null);
        return localContainerEntityManagerFactoryBean.getObject();*/
    }


/*    @Bean(name="org.springframework.context.annotation.internalPersistenceAnnotationProcessor")
    public PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor() {
        PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor = new PersistenceAnnotationBeanPostProcessor();
//        persistenceAnnotationBeanPostProcessor.setDefaultPersistenceUnitName();
        return persistenceAnnotationBeanPostProcessor;

        *//*return new PersistenceAnnotationBeanPostProcessor() {
            @Override
            public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
                super.postProcessBeforeDestruction(bean, beanName);
            }
        };*//*
    }*/


    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(emf);
        System.out.println("PlatformTransactionManager registered");

        return txManager;
    }
}
