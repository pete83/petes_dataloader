package com.pete.playground.dataloader.entities;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;

/**
 * Created by pete on 2014-12-30.
 */
@Entity
@Table(name = "USERS")
public class User {

    private Long id;
    private String name;
    private School school;

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "FIRST_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public User withId(final Long id) {
        this.id = id;
        return this;
    }

    public User withName(final String name) {
        this.name = name;
        return this;
    }

    public User withSchool(final School school) {
        this.school = school;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("id", id)
                .append("name", name)
                .append("school", school)
                .toString();
    }
}
