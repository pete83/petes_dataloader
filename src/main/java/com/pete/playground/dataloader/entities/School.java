package com.pete.playground.dataloader.entities;

import javax.persistence.*;

/**
 * Created by pete on 2014-12-30.
 */
@Entity
@Table(name = "SCHOOLS")
//@SecondaryTable(name="DUPA")
public class School {
    private Long id;
    private String name;

    @Id
    @GeneratedValue
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "SCHOOL_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public School withId(final Long id) {
        this.id = id;
        return this;
    }

    public School withName(final String name) {
        this.name = name;
        return this;
    }

}
