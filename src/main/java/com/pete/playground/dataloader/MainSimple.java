package com.pete.playground.dataloader;

import com.pete.playground.dataloader.entities.School;
import com.pete.playground.dataloader.entities.User;
import org.hibernate.ConnectionReleaseMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.internal.StandardServiceRegistryImpl;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.Target;

/**
 * @author pete
 *         Created on 2015-03-08.
 */
public class MainSimple {
    public static void main(String[] args) {
        Configuration configuration = new Configuration()
                .addAnnotatedClass(School.class)
                .addAnnotatedClass(User.class)
                .setProperty(Environment.DIALECT, "org.hibernate.dialect.Oracle10gDialect")
                .setProperty(Environment.URL,"jdbc:oracle:thin:@localhost:1521")
                .setProperty(Environment.DRIVER,"oracle.jdbc.driver.OracleDriver")
                .setProperty(Environment.USER,"direct_hibernate_export")
                .setProperty(Environment.PASS,"abc123")
                .setProperty(Environment.POOL_SIZE,"4")
                .setProperty(Environment.GENERATE_STATISTICS,"true")
                .setProperty(Environment.RELEASE_CONNECTIONS, ConnectionReleaseMode.AFTER_STATEMENT.name())
                ;



/*        SchemaExport schemaExport = new SchemaExport(configuration);
        schemaExport.setOutputFile("petesHibernateDDL.sql");
        schemaExport.create(true,true);*/

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(new School().withName("Trala"));
        transaction.commit();
        System.out.println("Done!");
        session.close();
        sessionFactory.close();
        System.out.println("Closed");
    }
}
