package com.pete.playground.dataloader.business;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Created by pete on 2015-01-17.
 */
@Component
public class CSVConfigurationReader {

    public Iterable<CSVRecord> readFile(String shortFileName) {
        Reader reader = null;
        try {
            reader = new FileReader(this.getClass().getClassLoader().getResource(shortFileName).getFile());
            return CSVFormat.DEFAULT.parse(reader);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to find file: " + shortFileName, e);
        } catch (IOException e) {
            throw new RuntimeException("Unable to read file: " + shortFileName, e);
        }
    }
}
