package com.pete.playground.dataloader.business;

import com.pete.playground.dataloader.beans.CurrentAirlineHolder;
import com.pete.playground.dataloader.beans.DataLoaderConfiguration;
import com.pete.playground.dataloader.entities.School;
import com.pete.playground.dataloader.entities.User;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Pete.Sterna@sabre.com
 */
@Component
public class DataLoaderServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataLoaderServices.class);

    @Autowired
    private DataLoaderConfiguration dataLoaderConfiguration;

    @Autowired
    private CSVConfigurationReader csvConfigurationReader;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String SCHOOL_CONFIGURATION_CSV_FILE_NAME = "school.csv";
    private static final String USER_CONFIGURATION_CSV_FILE_NAME = "user.csv";

    private static final String[] CSV_DATA_FILES_TO_LOAD = {SCHOOL_CONFIGURATION_CSV_FILE_NAME, USER_CONFIGURATION_CSV_FILE_NAME};

    /**
     * Lets try to keep everything in a single transaction. This way if ANYTHING goes wrong, all changes are rolled back.
     */
    @Transactional
    public void loadEntireDatabase() {
        LOGGER.info("Loading database for airline {}", CurrentAirlineHolder.getCurrentAirlineCode());
        validateConfigurationFiles();
        cleanDatabaseIfNecessary();
        Map<Long, School> schools = loadSchools();
        Map<Long, User> users = loadUsers(schools);
        schools.values().forEach(entityManager::persist);
        users.values().forEach(entityManager::persist);
        LOGGER.info("Database loaded for " + CurrentAirlineHolder.getCurrentAirlineCode());
    }

    private void validateConfigurationFiles() {
        LOGGER.info("Validating configuration files for airline {} ", CurrentAirlineHolder.getCurrentAirlineCode());
        Set<String> errorsEncountered = new HashSet<>();
        for (String airlineCode : dataLoaderConfiguration.getEnabledAirlines()) {
            for (String csvFileToLoad : CSV_DATA_FILES_TO_LOAD) {
                if (getClass().getClassLoader().getResource(airlineCode + "/" + csvFileToLoad) == null) {
                    errorsEncountered.add(String.format("* File %s could not be found for airline %s in the directory %s",
                            csvFileToLoad, airlineCode, airlineCode + "/" + csvFileToLoad));
                }
            }
        }
        if (errorsEncountered.size() > 0) {
            throw new DataLoaderConfigurationException(errorsEncountered);
        }
    }

    private void cleanDatabaseIfNecessary() {
        LOGGER.info("Cleaning the database for airline {}", CurrentAirlineHolder.getCurrentAirlineCode());
        int removedCount = entityManager.createQuery("delete from User").executeUpdate();
        LOGGER.info("{} users removed", removedCount);
        removedCount = entityManager.createQuery("delete from School").executeUpdate();
        LOGGER.info("{} schools removed", removedCount);
        LOGGER.info("The database has been successfully cleaned for airline {}", CurrentAirlineHolder.getCurrentAirlineCode());
    }

    public Map<Long, School> loadSchools() {
        LOGGER.info("Loading schools for " + CurrentAirlineHolder.getCurrentAirlineCode());
        Iterable<CSVRecord> csvRecords = csvConfigurationReader.readFile(CurrentAirlineHolder.getCurrentAirlineCode() + "/school.csv");
        Map<Long, School> schoolsToLoad = new HashMap<>();
        for (CSVRecord csvRecord : csvRecords) {
            schoolsToLoad.put(Long.parseLong(csvRecord.get(0)),
                    new School().withId(Long.parseLong(csvRecord.get(0)))
                            .withName(csvRecord.get(1)));
        }
        LOGGER.info("{} schools loaded for airline {}", schoolsToLoad.size(), CurrentAirlineHolder.getCurrentAirlineCode());
        return schoolsToLoad;
    }

    public Map<Long, User> loadUsers(Map<Long, School> schools) {
        LOGGER.info("Loading users for " + CurrentAirlineHolder.getCurrentAirlineCode());
        Iterable<CSVRecord> csvRecords = csvConfigurationReader.readFile(CurrentAirlineHolder.getCurrentAirlineCode() + "/user.csv");
        Map<Long, User> usersToLoad = new HashMap<>();
        for (CSVRecord csvRecord : csvRecords) {
            long userId = Long.parseLong(csvRecord.get(0));
            String userName = csvRecord.get(1);
            long schoolId = Long.parseLong(csvRecord.get(2));
            usersToLoad.put(userId,
                    new User().withId(userId)
                            .withName(userName).withSchool(
                            schools.get(schoolId)));
        }
        LOGGER.info("{} users loaded for airline {}", usersToLoad.size(), CurrentAirlineHolder.getCurrentAirlineCode());
        return usersToLoad;
    }
}
