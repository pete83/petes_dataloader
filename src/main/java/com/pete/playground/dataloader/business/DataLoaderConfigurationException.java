package com.pete.playground.dataloader.business;

import org.apache.commons.lang3.StringUtils;

import java.util.Set;

/**
 * @author pete
 *         Created on 2015-01-18.
 */
public class DataLoaderConfigurationException extends RuntimeException {
    public DataLoaderConfigurationException(Set<String> encounteredErrors) {
        super("Errors encountered when reading data loader configuration files: \n" + StringUtils.join(encounteredErrors, "\n"));
    }
}
