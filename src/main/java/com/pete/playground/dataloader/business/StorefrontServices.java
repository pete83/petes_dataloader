package com.pete.playground.dataloader.business;

import com.pete.playground.dataloader.entities.User;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by pete on 2014-12-30.
 */
@Component
public class StorefrontServices {
    @PersistenceContext
    EntityManager entityManager;

    public void hitTheDatabase() {
        System.out.println("Entity manager: " + entityManager);
        List<User> users = entityManager.createQuery("select u from User u", User.class).getResultList();
        System.out.println("Found users: " + users);
    }


}
