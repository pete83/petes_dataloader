
    drop table DUPA cascade constraints

    drop table SCHOOLS cascade constraints

    drop table USERS cascade constraints

    drop sequence hibernate_sequence

    create table DUPA (
        ID number(19,0) not null,
        primary key (ID)
    )

    create table SCHOOLS (
        ID number(19,0) not null,
        SCHOOL_NAME varchar2(255 char),
        primary key (ID)
    )

    create table USERS (
        ID number(19,0) not null,
        FIRST_NAME varchar2(255 char),
        school_ID number(19,0),
        primary key (ID)
    )

    alter table DUPA 
        add constraint FK_n6joktrewg7t9ml52fbuyx1rd 
        foreign key (ID) 
        references SCHOOLS

    alter table USERS 
        add constraint FK_dw9l6wxk4giqwsbydcp51o1id 
        foreign key (school_ID) 
        references SCHOOLS

    create sequence hibernate_sequence
